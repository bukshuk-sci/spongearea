﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;

using NLog;

namespace SpongeArea
{
    internal class Program
    {
        static Program()
        {
            Logger = LogManager.GetCurrentClassLogger();
        }

        private static Logger Logger { get; }

        private static void Main()
        {
            try
            {
                Run(true);

                Thread.Sleep(1500);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Error(ex.Message);
            }
        }

        public static void Run(bool doParallel)
        {
            var fromFolder = GetFromFolder();
            var toFile = GetToFile(fromFolder);

            var composer = new Composer(new List<Color>
            {
                Color.White,
                Color.Black,
                Color.Red,
                Color.Lime,
                Color.Blue,
                Color.Cyan,
                Color.Magenta,
                Color.Yellow,
                Color.Maroon,
                Color.SaddleBrown,
                Color.Crimson,
                Color.OrangeRed,
                Color.LightSalmon,
                Color.Navy,
                Color.Indigo,
                Color.Purple,
                Color.Pink,
                Color.Gray,
                Color.Teal,
                Color.Green,
                Color.Olive,
                Color.Khaki
            });

            composer.Play(fromFolder, toFile, doParallel, Console.WriteLine);
        }

        private static string GetFromFolder()
        {
            var fromFolder = Settings.Default.FromFolder;

            if (!Directory.Exists(fromFolder))
            {
                Logger.Info("The [{0}] folder does not exist", fromFolder);

                fromFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fromFolder);

                if (!Directory.Exists(fromFolder))
                {
                    Logger.Info("The [{0}] folder does not exist", fromFolder);

                    throw new ApplicationException("Cannot find folder with pictures");
                }
            }

            return fromFolder;
        }

        private static string GetToFile(string fromFolder)
        {
            var baseDirName = Path.GetDirectoryName(Path.GetFullPath(fromFolder));
            var toFile = Path.Combine(baseDirName, Settings.Default.ToFile);

            return toFile;
        }
    }
}